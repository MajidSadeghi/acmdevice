#ifndef PACKET_H
#define PACKET_H

#include "acmobject.h"

enum PacketStatus{
    PS_Valid,
    PS_CheckSumFailure,
    PS_DataLost,
    PS_HeaderFailure,
    PS_Unknown
};

enum Command{
    Command_GatewayName = 0x00,
    Command_AddCard = 0x01,
    Command_RemoveCard = 0x02,
    Command_RemoveAllCards = 0x03,
    Command_SetGatewayStatus = 0x04,
    Command_SetGatewayDateTime = 0x05,
    Command_GetGatewayDateTime = 0x06,
    Command_SendLogAck = 0x07,
    Command_SetGatewayId = 0x08,
    Command_GetGatewayId = 0x09,
    Command_None = 0x10
};

enum GatewayAction{
    GatewayAction_Normal = 0,
    GatewayAction_OpenDoor = 1,
    GatewayAction_CloseForever = 2,
    GatewayAction_OpenForever = 3
};

class Packet : public ACMObject
{
public:
    Packet();
    Packet(char* data);
    Packet(Packet& packet);
    Packet(char cmd, char dir, char* data);

    char* toByteArray();
    char checksum();
    char *toHexByteArray();

    static const char* charToHex(char c);
    static const char* intToHex(int i);
    static PacketStatus checkPacketDataFormat(char* data);
    static char checksum(char *data);
    static PacketStatus parseData(char *data, Packet *acket);
    static char* toByteArray(Packet *packet);
    static Packet* genAddCradCommand(char* cardNumber);
    static Packet* genRemoveCardCommand(char* cardNUmber);
    static Packet* genRemoveAllCardsCommand();
    static Packet* genGatewayActionCommand(GatewayAction action);
    static Packet* genSetGatewayDateTimeCommand(QDateTime dateTime);
    static Packet* getGetGatewayDateTimeCommand();
    static Packet* genSendGatewayLogAckCommand();
    static Packet* genSetGatewayIdCommand(short id);
    static Packet* getGatewayIdCommand();

    char* getData();
    char getCS();
    char getNData();
    char getCMD();
    char getDir();

protected:
    void log();

private:

    static const char mSOT = 0xF8;
    static const char mEOT = 0xF9;
    char mCmd;
    short mNData;
    char mCS;
    char mDir;
    char *mData;
};

#endif // PACKET_H
