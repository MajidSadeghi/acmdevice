#ifndef ACMOBJECT_H
#define ACMOBJECT_H

#include <QObject>
#include <QMessageLogger>
#include <QDebug>

class ACMObject : public QObject
{
    Q_OBJECT
public:
    explicit ACMObject(QString name, QObject *parent = 0);
    virtual void log() = 0 ;

protected:
    QString mObjectName;

signals:
    void propertyChanged(QString propertyName, QString val);

public slots:
    void freezData();
    void onPropertyChanged(QString propertyName, QString val);

};

#endif // ACMOBJECT_H
