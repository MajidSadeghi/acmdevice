#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>
#include <QtNetwork/QTcpSocket>
#include <QDateTime>
#include <acmobject.h>
#include <packet.h>

class Device : public ACMObject
{
    Q_OBJECT
public:
    Device(QString host, quint16 port);
    void connectToDevice();
    void log();
    void recvLogPacket(Packet *packet);
public slots:
    void clientDisconnected();
    void readClient();
    void clientConnected();
    void clientStateChanged(QAbstractSocket::SocketState state);
    void clientError(QAbstractSocket::SocketError error);
    void setDeviceToNormalMode();
    void openDevice();
    void openDeviceForever();
    void closeDeviceForever();
signals:
    void disconnected();
    void connected();
    void newLog(QString cardNumber, QDateTime dateTime, short deviceId);
private:
    QTcpSocket mTcpClient;
    QString mHost;
    quint16 mPort;
};

#endif // DEVICE_H
