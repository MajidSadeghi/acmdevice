#include <QCoreApplication>
#include <QString>
#include <QDebug>
#include <iostream>
#include "packet.h"
#include <device.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Packet::genSendGatewayLogAckCommand()->toHexByteArray();
    Device *device = new Device("192.168.1.6", 2000);
    device->connectToDevice();
    return a.exec();
}
