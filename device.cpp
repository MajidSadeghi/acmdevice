#include "device.h"
#include <iostream>
#include <sstream>
#include <QMessageLogger>

using namespace std;
Device::Device(QString host, quint16 port): ACMObject("Device"), mHost(host), mPort(port)
{
    connect(&mTcpClient, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
    connect(&mTcpClient, SIGNAL(readyRead()), this, SLOT(readClient()));
    connect(&mTcpClient, SIGNAL(connected()), this, SLOT(clientConnected()));
    connect(&mTcpClient, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(clientStateChanged(QAbstractSocket::SocketState)));
    connect(&mTcpClient, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(clientError(QAbstractSocket::SocketError)));
}

void Device::connectToDevice(){
    mTcpClient.abort();
    mTcpClient.connectToHost(mHost, mPort);
}

void Device::clientConnected(){
    if(DEBUG)
        QMessageLogger(__FILE__, __LINE__, 0).debug() << "Connected to the host : " << mHost << " port: " << mPort;
    emit connected();
    setDeviceToNormalMode();
}

void Device::clientDisconnected(){
    if(DEBUG)
        QMessageLogger(__FILE__, __LINE__, 0).debug() << "disConnected from the host : " << mHost << " port: " << mPort;
    emit disconnected();
}

void Device::clientError(QAbstractSocket::SocketError error){
    if(DEBUG)
        QMessageLogger(__FILE__, __LINE__, 0).debug() << "client socket error : " << error << ", " << mTcpClient.errorString();
}

void Device::clientStateChanged(QAbstractSocket::SocketState state){
    if(DEBUG)
        QMessageLogger(__FILE__, __LINE__, 0).debug() << "client state changed : " << state;
}

void Device::readClient(){
    char buffer[1024] = {0};
    mTcpClient.read(buffer, mTcpClient.bytesAvailable());
    if(Packet::checkPacketDataFormat(buffer) == PS_Valid){
        Packet *packet = new Packet(buffer);
        if(packet->getCMD() == Command_SetGatewayStatus){
            packet->toHexByteArray();
        }
        else if(packet->getCMD() == Command_SendLogAck){
            recvLogPacket(packet);
        }
    }
}

void Device::recvLogPacket(Packet *packet){
    std::string cardNumber;
    stringstream ss;
    short deviceId =  (unsigned short)(packet->getData()[0]) * 100 + ((unsigned short)(packet->getData()[1])>255 ? 0 : (unsigned short)(packet->getData()[1]));
    for(short i = 0; i < 10 ; i++){
        ss << ( packet->getData()[i+2] - '0');
        if( packet->getData()[i+2] < 0)
            return;
    }
    ss >> cardNumber;
    unsigned short year = (unsigned short)packet->getData()[12];
    unsigned short month = (unsigned short)packet->getData()[13];
    unsigned short day = (unsigned short)packet->getData()[14];
    unsigned short hour = (unsigned short)packet->getData()[15];
    unsigned short minute = (unsigned short)packet->getData()[16];
    unsigned short second = (unsigned short)packet->getData()[17];
    QDateTime dateTime(QDate(year, month, day), QTime(hour, minute, second));
    QString card(QString::fromStdString(cardNumber));
    if(DEBUG){
        QMessageLogger(__FILE__, __LINE__, 0).debug() << "Log :device id: " << deviceId << " Card : " << card << " : " << year << "/" << month << "/" << day << " " << hour <<":" << minute <<":"<<second;
    }
    char *data = Packet::genSendGatewayLogAckCommand()->toByteArray();
    mTcpClient.write(data, 6);
    mTcpClient.flush();
    emit newLog(card, dateTime, deviceId);
}

void Device::setDeviceToNormalMode(){
    char data[7] = {0xF8, 0x00, 0x04, 0x01, 0x00, 0xFA, 0xF9};
    mTcpClient.write(data, 7);
    mTcpClient.flush();
}

void Device::openDevice(){
    Packet::genGatewayActionCommand(GatewayAction_OpenDoor)->toHexByteArray();
    char *data = Packet::genGatewayActionCommand(GatewayAction_OpenDoor)->toByteArray();
    mTcpClient.write(data, 7);
    mTcpClient.flush();
}

void Device::openDeviceForever(){
    Packet::genGatewayActionCommand(GatewayAction_OpenForever)->toHexByteArray();
    char *data = Packet::genGatewayActionCommand(GatewayAction_OpenForever)->toByteArray();
    mTcpClient.write(data, 7);
    mTcpClient.flush();
}

void Device::closeDeviceForever(){
    Packet::genGatewayActionCommand(GatewayAction_CloseForever)->toHexByteArray();
    char *data = Packet::genGatewayActionCommand(GatewayAction_CloseForever)->toByteArray();
    mTcpClient.write(data, 7);
    mTcpClient.flush();
}

void Device::log(){

}
