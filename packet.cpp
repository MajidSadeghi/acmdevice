#include "packet.h"
#include <sstream>
#include <iostream>
using namespace std;
Packet::Packet(): ACMObject("PacketObject")
{
    mDir = -1;
    mCmd = 0;
    mCS = 0;
    mNData = 0;
}

Packet::Packet(char *data): ACMObject("PacketObject")
{
    mDir = data[1];
    mCmd = data[2];
    mNData = data[3];
    mData = new char[mNData];
    memcpy(mData, data+4, mNData);
    mCS = data[mNData + 4];
}

Packet::Packet(Packet &packet): ACMObject("PacketObject")
{
    mCmd = packet.getCMD();
    mCS = packet.getCS();
    mNData = packet.getNData();
    mData = packet.getData();
    mDir = packet.getDir();
}

Packet::Packet(char cmd, char dir, char *data): ACMObject("PacketObject"){
    mCmd = cmd;
    mDir = dir;
    if(data){
        mNData = strlen(data);
        mData = new char[mNData];
        memcpy(mData, data, mNData);
    }
    else
        mNData = 0;
    mCS = checksum();
}

void Packet::log(){
    if(DEBUG){
        QMessageLogger(__FILE__, __LINE__, 0).debug() << " Dir: " << QString("%1").arg(mDir , 0, 16) << " CMD: " <<  QString("%1").arg(mCmd , 0, 16) << " CS: " << QString("%1").arg(mCS , 0, 16);
        if(mData){
            QMessageLogger(__FILE__, __LINE__, 0).debug() << " Length: " << QString("%1").arg(strlen(mData) , 0, 10);
            QString data = "";
            for(int i = 0 ; i < strlen(mData); i++){
                data += QString("%1").arg(mData[i] , 0, 16);
            }
            QMessageLogger(__FILE__, __LINE__, 0).debug() << data;
        }
    }
}

PacketStatus Packet::checkPacketDataFormat(char *data){
    try{
        short length = strlen(data);
        if(data[0] == mSOT && data[length - 1] == mEOT){
            if(data[3] + 6 == length){
                if(checksum(data) == data[data[3] + 4]){
                    return PS_Valid;
                }
                else{
                    return PS_CheckSumFailure;
                }
            }
            else{
                return PS_DataLost;
            }
        }
        else{
            return PS_HeaderFailure;
        }
    }
    catch(...){
        qDebug() << " Segmentation fault in " << __FILE__ << " : " << __LINE__;
    }
}

char Packet::checksum(char *data){
    char sum = 0x00;
    int temp = 0;
    short length = strlen(data);
    for(int i = 1 ;i < length - 2; i++){
        temp =  sum + (int)data[i];
        temp = temp > 255 ? temp - 256 : temp;
        sum = (char)temp;
    }
    return (255 - sum);
}

char Packet::checksum(){
    char sum = 0x00;
    int temp = 0;
    sum = mDir + mCmd + mNData;
    for(int i = 0 ;i < mNData; i++){
        temp =  sum + (int)mData[i];
        temp = temp > 255 ? temp - 256 : temp;
        sum = (char)temp;
    }
    return (255 - sum );
}

PacketStatus Packet::parseData(char *data, Packet *packet){
    PacketStatus packetStaus = Packet::checkPacketDataFormat(data);
    if(packetStaus != PS_Valid)
        return packetStaus;
    else{
        packet = new Packet(data);
        return PS_Valid;
    }
}

char* Packet::toHexByteArray(){
    char * result = new char[(6+mNData)*2];
    char * data = toByteArray();
    for(short i = 0; i < 6+mNData  ;i++){
        const char* hexed = charToHex(data[i]);
        std::cout << hexed << ",";
    }
    return result;
}

const char* Packet::charToHex(char c){
    std::stringstream stream;
    stream << std::hex << (int)c;
    std::string result( stream.str() );
    return result.c_str();
}

const char* Packet::intToHex(int i){
    std::stringstream stream;
    stream << std::hex << i;
    std::string result( stream.str() );
    return result.c_str();
}

char* Packet::toByteArray(){
    char *result = new char[this->mNData + 6];
    short index = 0 ;
    result[index++] = 0xF8;
    result[index++] = mDir;
    result[index++] = mCmd;
    result[index++] = mNData;
    for(short i = 0 ; i < mNData ; i++){
        result[index++] = mData[i];
   }
    result[index++] = mCS;
    result[index] = 0xF9;
    return result;
}

Packet* Packet::genRemoveAllCardsCommand(){
    return new Packet(Command_RemoveAllCards, 0, NULL);
}

Packet* Packet::genAddCradCommand(char *cardNumber){
    return new Packet(Command_AddCard, 0, cardNumber);
}

Packet* Packet::genRemoveCardCommand(char *cardNumber){
     return new Packet(Command_RemoveCard, 0, cardNumber);
}

Packet* Packet::genGatewayActionCommand(GatewayAction action){
    char data[2] = {action, '\0'};
    return new Packet(Command_SetGatewayStatus, 0, data);
}

Packet* Packet::genSendGatewayLogAckCommand(){
     return new Packet(Command_SendLogAck, 0, NULL);
}

char* Packet::toByteArray(Packet *packet){
    return packet->toByteArray();
}

char* Packet::getData(){
    return mData;
}

char Packet::getCS(){
    return mCS;
}

char Packet::getNData(){
    return mNData;
}

char Packet::getCMD(){
    return mCmd;
}

char Packet::getDir(){
    return mDir;
}
