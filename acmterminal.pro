#-------------------------------------------------
#
# Project created by QtCreator 2016-01-01T12:07:30
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = acmterminal
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

DEFINES += "DEBUG=true"

SOURCES += main.cpp \
    acmobject.cpp \
    packet.cpp \
    devicemanager.cpp \
    device.cpp

HEADERS += \
    acmobject.h \
    packet.h \
    devicemanager.h \
    device.h
