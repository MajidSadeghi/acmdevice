#include "acmobject.h"

ACMObject::ACMObject(QString name, QObject *parent) :
    QObject(parent),mObjectName(name)
{
    if(DEBUG)
        QMessageLogger(__FILE__, __LINE__, 0).debug() << " Object: " << name << " Created";
    connect(this, SIGNAL(propertyChanged(QString, QString)), this, SLOT(onPropertyChanged(QString,QString)));
}

void ACMObject::freezData(){
    if(DEBUG)
        this->log();
}

void ACMObject::onPropertyChanged(QString propertyName, QString val){
    if(DEBUG)
        QMessageLogger(__FILE__, __LINE__, 0).debug() << " Object: " << mObjectName << ", Property :" << propertyName << ", val: " << val;
}
